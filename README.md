[![pipeline status](https://gitlab.com/ag-projects/icesmooth/badges/main/pipeline.svg)](https://gitlab.com/ag-projects/icesmooth/-/commits/main)

# Describing places

A web presentation on vocabulary for describing places.
Quick access link: [Advanced adjectives describing places](https://arturgulik.gitlab.io/mini-vocabulary-presentation-describing-places/)

Author: Artur Gulik
Used framework: [icesmooth](https://gitlab.com/ArturGulik/icesmooth)
