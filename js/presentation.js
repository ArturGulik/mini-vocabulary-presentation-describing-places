config.assignId = false;
config.comp = {
	on: false,
	prefixes: ['-webkit-', '-moz-', '']
}
let ice = new Presentation();

let openingSlide = new Slide();
openingSlide.addStyle('lefted');
openingSlide.addStyle('bottom');
openingSlide.chState(0);
let titleStyle = 'text-align: left;\
color: black;\
font-size: 95px;\
padding-left: 20px;';

openingSlide.pushElement(
  'div=Advanced adjectives', '',
  titleStyle);
openingSlide.pushElement(
  'describing places', '',
  titleStyle);

openingSlide.pushElement(
  'div=Artur Gulik',
  '',
  'color: black;\
font-size: 30px;\
text-align: left;\
padding-left: 20px;\
padding-bottom: 20px');


function mkvslide(word, definition, pronounciation, example, imgsrc){
  let slide = new Slide();
  slide.setTitle(
    word + ' <span style="font-size: 40%">' + pronounciation + '</span>',
    'text-align: left;\
padding-left: 20px;\
font-size: 70px;');
  slide.setSubTitle('','display:block;');
  slide.adjust = true;
  slide.addStyle('middle');
  slide.addStyle('centered');
  slide.addContainerStyle('full-height');
  slide.addContainerStyle('full-width');
  slide.chSection(0);
  slide.chState(1);
  let common = 'position: relative; display: inline-block; font-size: 29px;';
  let commoni = common+'margin-top: 3%; font-style: italic;';
  let is = common + 'margin-top: 5%; margin-bottom: 5%; max-width: 60%; height: auto; max-height: 50%;';
  let bef = 'transform: translateY(0%);';
  let aft = 'transform: translateY(0%);';
  slide.pushElement(definition, common + 'opacity: 0;',
		    common + 'opacity: 1;');
  slide.pushElement(example, commoni + 'opacity: 0;',
		    commoni + 'opacity: 0;');
  slide.pushElement('img=img/' + imgsrc, is + 'opacity: 0;',is + 'opacity: 0;');
  slide.chState(2);
  slide.pushElement(definition, common, common);
  slide.pushElement(example, commoni + 'opacity: 0;',
		    commoni + 'opacity: 1;');
  slide.pushElement('img=img/' + imgsrc, is + bef + 'opacity: 0;',
		    is + aft + 'opacity: 0;');

  slide.chState(3);
  slide.pushElement(definition, common, common);
  slide.pushElement(example, commoni, commoni);
  slide.pushElement('img=img/' + imgsrc, is + bef + 'opacity: 0;',
		    is + aft + 'opacity: 1;');
  return slide;
}
let words = [
  {
    word: 'vast',
    pronounciation: 'GB/vɑːst/ US/væst/',
    definition: 'extremely large in area, size, amount, etc.',
    example: 'The City of New York is a vast metropolitan region, occupying a total of 8,683 square kilometers.',
    imgsrc: 'vast.jpg'
  },
  {
word: 'sprawling',
pronounciation: 'GB /ˈsprɔː.lɪŋ/ US /ˈsprɑː.lɪŋ/',
definition: '(of a city) covered with buildings across a large area, often ones that have been added gradually over a period of time',
example: 'Somewhere in this sprawling metropolis, there is a secret military laboratory.',
imgsrc: 'sprawling.jpg'
},
{
word: 'ramshackle',
pronounciation: '/răm′shăk′əl/',
definition: 'So poorly constructed or kept up that disintegration is likely ; badly constructed or maintained',
example: 'There\'s a ramshackle old shed at the bottom of the garden.',
imgsrc: 'ramshackle.jpg'
},
{
word: 'quaint',
  pronounciation: '/kweɪnt/',
definition: 'attractive because of being unusual and especially old-fashioned',
example: 'In Spain, we visited a cobblestone plaza with quaint little cafés around its perimeter.',
imgsrc: 'quaint.jpg'
},
{
word: 'windswept',
pronounciation: '/ˈwɪnd.swept/',
definition: '(of places) open to and not protected from strong winds',
example: 'We drove down to the windswept Atlantic coast of Portugal.',
  imgsrc: 'windswept.jpg'
},
{
word: 'bleak',
pronounciation: '/bliːk/',
definition: 'If a place is bleak, it is empty, and not welcoming or attractive',
example: 'The house stands on a bleak, windswept hilltop.',
imgsrc: 'bleak.jpg'
},
  {
  word: 'unspoiled',
  pronounciation: '/ʌnˈspɔɪld/ /ʌnˈspɔɪlt/',
  definition: 'An unspoiled place is beautiful because it has not been changed or damaged by people',
  example: 'We owe them a debt of gratitude that that lovely area remains comparatively unspoiled, even to this day.',
  imgsrc: 'unspoiled.jpg'
}
];

ice.slides = [openingSlide];
words.forEach((word) =>
  ice.slides.push(mkvslide(word.word, word.definition,
			   word.pronounciation, word.example, word.imgsrc)));

ice.setBackgrounds(['img/beach_drink.jpg', '', '','', '','','','']);

ice.slides.forEach((s) => s.createHTML());

ice.init();




//A slide is a Slide object with two lists of states (for and back States)

